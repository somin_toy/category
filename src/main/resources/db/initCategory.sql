-- ================ Root 카테고리
insert into categories(name, parent) values ('럭셔리', 0l);
insert into categories(name, parent) values ('뷰티', 0l);
insert into categories(name, parent) values ('스포츠', 0l);


-- =============== Level 2 카테고리

insert into categories(name, parent) values ('전체', 1l);
insert into categories(name, parent) values ('상의', 1l);
insert into categories(name, parent) values ('하의', 1l);

insert into categories(name, parent) values ('전체 상품', 2l);
insert into categories(name, parent) values ('립', 2l);
insert into categories(name, parent) values ('베이스', 2l);

insert into categories(name, parent) values ('러닝', 3l);
insert into categories(name, parent) values ('피트니스', 3l);

-- =============== Level 3 카테고리

insert into categories(name, parent) values ('로드화', 10l);
insert into categories(name, parent) values ('트레일화', 10l);

insert into categories(name, parent) values ('상의', 11l);
insert into categories(name, parent) values ('하의', 11l);
insert into categories(name, parent) values ('신발', 11l);
