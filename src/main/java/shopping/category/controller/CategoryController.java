package shopping.category.controller;

import lombok.RequiredArgsConstructor;
import shopping.category.Entity.CategoryEntity;
import shopping.category.common.JsonResult;
import shopping.category.common.ResultCode;
import shopping.category.dto.Category;
import shopping.category.exception.BusinessException;
import shopping.category.service.CategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/category")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping
    public ResponseEntity<JsonResult> categories(@RequestParam(required = false) Long id) {

        try {
            List<Category> data = categoryService.categories(id);
            return ResponseEntity.ok(JsonResult.createJsonResult(ResultCode.OK, data));
        } catch (BusinessException e) {
            return ResponseEntity.ok(JsonResult.createJsonResult(e.getErrorCode()));
        }
    }

    @PostMapping
    public ResponseEntity<JsonResult> categoryAdd(@RequestParam String name, @RequestParam Long parent) {

        try {
            categoryService.add(name, parent);
            return ResponseEntity.ok(JsonResult.createJsonResult(ResultCode.OK, ""));
        } catch (BusinessException e) {
            return ResponseEntity.ok(JsonResult.createJsonResult(e.getErrorCode()));
        }
    }

    @DeleteMapping
    public ResponseEntity<JsonResult> categoryRemove(@RequestParam Long id) {

        try {
            categoryService.remove(id);
            return ResponseEntity.ok(JsonResult.createJsonResult(ResultCode.OK, ""));
        } catch (BusinessException e) {
            return ResponseEntity.ok(JsonResult.createJsonResult(e.getErrorCode()));
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity<JsonResult> categoryModify(@PathVariable Long id,
                                                     @RequestParam(required = false) String name,
                                                     @RequestParam(required = false) Long parent){

        try {
            CategoryEntity data = categoryService.modify(id, name, parent);
            return ResponseEntity.ok(JsonResult.createJsonResult(ResultCode.OK, data));
        } catch (BusinessException e) {
            return ResponseEntity.ok(JsonResult.createJsonResult(e.getErrorCode()));
        }
    }

    //test
}
