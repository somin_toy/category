package shopping.category.exception;

import shopping.category.common.ResultCode;

public class BusinessException extends RuntimeException {

    private ResultCode errorCode;

    public BusinessException(ResultCode errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public ResultCode getErrorCode() {
        return errorCode;
    }

}
