package shopping.category.service;

import shopping.category.Entity.CategoryEntity;
import shopping.category.common.ResultCode;
import shopping.category.dto.Category;
import shopping.category.exception.BusinessException;
import shopping.category.repository.CategoryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

@Service
@Transactional
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private static Map<Long, List<Category>> allCategory;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public List<Category> categories(Long id) {

        Category root = mappingCategory(id);
        
        allCategory = findAllCategory();
        findSubCategory(root);

        return root.getSubCategories();
    }

    private void findSubCategory(Category parent) {

        List<Category> subCategory = allCategory.get(parent.getId());
        if (subCategory == null)
            return;

        parent.setSubCategories(subCategory);

        subCategory.stream().forEach(sub -> {
            findSubCategory(sub);
        });
    }

    private Map<Long, List<Category>> findAllCategory() {
        return categoryRepository.findAll()
                .stream()
                .map(c -> new Category(c.getId(), c.getName(), c.getParent()))
                .collect(groupingBy(cd -> cd.getParent()));
    }

    private Category mappingCategory(Long id) {
        if (id == null || id == 0l) {
            return Category.builder()
                    .id(0l)
                    .name("ROOT")
                    .build();
        }else {
            CategoryEntity c = categoryRepository.findById(id).orElseThrow(() -> {
                throw new BusinessException(ResultCode.NOT_EXIST_CATEGORY);
            });

            return Category.builder()
                    .id(c.getId())
                    .name(c.getName())
                    .parent(c.getParent())
                    .build();
        }
    }

    public void add(String name, Long parent) {

        categoryRepository.findById(parent).orElseThrow(() -> {
            throw new BusinessException(ResultCode.NOT_EXIST_PARENT);
        });

        categoryRepository.save(new CategoryEntity(name, parent));
    }

    public void remove(Long id) {
        categoryRepository.findById(id).orElseThrow(() -> {
            throw new BusinessException(ResultCode.NOT_EXIST_CATEGORY);
        });

        categoryRepository.deleteById(id);
    }

    public CategoryEntity modify(Long id, String name, Long parent) {
        CategoryEntity category = categoryRepository.findById(id).orElseThrow(() -> {
            throw new BusinessException(ResultCode.NOT_EXIST_CATEGORY);
        });

        if (name != null && !name.isEmpty()) {
            category.setName(name);
        }

        if (parent != null) {

            if (0 < parent) {
                categoryRepository.findById(parent).orElseThrow(() -> {
                    throw new BusinessException(ResultCode.NOT_EXIST_PARENT);
                });
            }

            category.setParent(parent);
        }

        categoryRepository.save(category);

        return category;
    }
}