package shopping.category.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Category {
    private Long id;
    private String name;
    private Long parent;
    private List<Category> subCategories;

    @Builder
    public Category(Long id, String name, Long parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }
}