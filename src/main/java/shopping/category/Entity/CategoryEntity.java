package shopping.category.Entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "CATEGORIES")
public class CategoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Long parent;

    @Builder
    public CategoryEntity(String name, Long parent) {
        this.name = name;
        this.parent = parent;
    }
}