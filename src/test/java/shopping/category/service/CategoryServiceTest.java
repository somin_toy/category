package shopping.category.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import shopping.category.Entity.CategoryEntity;
import shopping.category.common.JsonResult;
import shopping.category.common.ResultCode;
import shopping.category.dto.Category;
import shopping.category.exception.BusinessException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class CategoryServiceTest {

    @Autowired
    private CategoryService categoryService;

    private static ObjectMapper mapper = new ObjectMapper();

    @Test
    void 전체_카테고리_조회() {

        try {
            //given

            //when
            List<Category> data = categoryService.categories(null);

            //then
            // 최상위 카테고리의 갯수는 3
            assertThat(data.size()).isEqualTo(3);

            JsonResult result = JsonResult.createJsonResult(ResultCode.OK, data);
            System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result));

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    void 하위_카테고리_조회() {

        try {
            //given
            Long id = 10l;
            //when
            List<Category> data = categoryService.categories(id);

            //then
            assertThat(data.size()).isEqualTo(2);

            JsonResult result = JsonResult.createJsonResult(ResultCode.OK, data);
            System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result));

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    @Test
    void 하위_카테고리_조회_에러() {
        // 존재하지 않는 카테고리 id
        try {
            //given
            Long id = 100l;

            //when
            List<Category> data = categoryService.categories(id);
        } catch (BusinessException e) {
            assertThat(e).isInstanceOf(BusinessException.class);
            assertThat(e.getErrorCode()).isEqualTo(ResultCode.NOT_EXIST_CATEGORY);
        }
    }

    @Test
    void 카테고리_추가(){
        //given
        Long parent = 11l;
        String name = "테스트";

        //when
        categoryService.add(name, parent);

        //then
        Category category = categoryService.categories(parent)
                .stream()
                .filter(c -> c.getName().equals(name))
                .findFirst()
                .orElse(null);

        assertThat(category.getName()).isEqualTo(name);

    }

    @Test
    void 카테고리_추가_에러(){
        // 존재하지 않는 parent
        try{
            //given
            Long parent = 100l;
            String name = "테스트";

            //when
            categoryService.add(name, parent);
        } catch (BusinessException e) {
            //then
            assertThat(e).isInstanceOf(BusinessException.class);
            assertThat(e.getErrorCode()).isEqualTo(ResultCode.NOT_EXIST_PARENT);
        }
    }

    @Test
    void 카테고리_삭제(){
            //given
            Long id = 1l;

            //when
            categoryService.remove(id);

            //then
            Category category = categoryService.categories(0l)
                    .stream()
                    .filter(c -> c.getId().equals(id))
                    .findFirst()
                    .orElse(null);

            assertThat(category).isEqualTo(null);
    }

    @Test
    void 카테고리_삭제_에러(){
        // 존재하지 않는 카테고리 id
        try {
            //given
            Long id = 100l;

            //when
            categoryService.remove(id);
        } catch (BusinessException e) {
            //then
            assertThat(e).isInstanceOf(BusinessException.class);
            assertThat(e.getErrorCode()).isEqualTo(ResultCode.NOT_EXIST_CATEGORY);
        }
    }

    @Test
    void 카테고리_수정() {
        //given
        Long id = 10l;
        String name = "테스트";
        Long parent = 0l;

        //when
        CategoryEntity data = categoryService.modify(id, name, parent);

        //then
        Category category = categoryService.categories(parent)
                .stream()
                .filter(c -> c.getName().equals(name))
                .findFirst()
                .orElse(null);

        assertThat(category.getName()).isEqualTo(name);
    }

    @Test
    void 카테고리_수정_에러_ID() {
        // 존재하지 않는 카테고리 id
        try {
            //given
            Long id = 100l;
            String name = "테스트";
            Long parent = 0l;

            //when
            CategoryEntity data = categoryService.modify(id, name, parent);
        } catch (BusinessException e) {
            //then
            assertThat(e).isInstanceOf(BusinessException.class);
            assertThat(e.getErrorCode()).isEqualTo(ResultCode.NOT_EXIST_CATEGORY);
        }
    }

    @Test
    void 카테고리_수정_에러_Parent() {
        // 존재하지 않는 parent
        try {
            //given
            Long id = 1l;
            String name = "테스트";
            Long parent = 100l;

            //when
            CategoryEntity data = categoryService.modify(id, name, parent);
        } catch (BusinessException e) {
            //then
            assertThat(e).isInstanceOf(BusinessException.class);
            assertThat(e.getErrorCode()).isEqualTo(ResultCode.NOT_EXIST_PARENT);
        }
    }

}