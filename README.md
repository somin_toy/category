# Category API
<br/><br/>

# 설치 가이드
<br/>

## 1 jar 파일 생성 방법

```
IntelliJ 사용시 

Gradel > build > bootJar 실행
프로젝트 디렉토리 ...\build\libs 에 jar 파일 생성 됨 
```

```
cmd 사용시

Gradle 설치
cmd 창에서 프로젝트의 디렉토리 경로에 접근 후 아래 명령어 실행
...\shopping\category> gradlew clean build 
프로젝트 디렉토리 ...\build\libs 에 jar 파일 생성 됨
```
## 2 CensOS 7 사용시 이용 방법

### 1. Package Manager 업데이트
```
$ sudo yum -y update
```

### 2. Java 설치 (openjdk version "11")
```
$ cd /usr/local
$ yum install -y java-11-openjdk-devel.x86_64
```
### 3. 정상 설치 확인
```
$ java -version
$ javac -version
```

### 4. 리눅스 서버에 jar 파일 복사
```
filezilla 를 사용하여 리눅스 서버로 jar 파일 복사
```

### 5. jar 파일 위치의 디렉토리에서 아래 명력어 실행
```
$ java -jar category-0.0.1-SNAPSHOT.jar
```

<br/><br/>

# API 가이드
<br/><br/>

## 카테고리 조회

### 요청
```
GET http://127.0.0.1:8081/category?id={id}
```

#### 요청 파라미터
|필드명|타입| 필수여부 | 설명                                                                       |
|---|---|------|--------------------------------------------------------------------------|
|id|Long| N    | 조회를 원하는 최상위 카테고리 ID를 입력해 주세요. <br/> 카테고리 ID를 입력하지 않으면 전체 카테고리 목록을 조회합니다. |

### 응답
요청이 성공하면, 응답 바디에 카테고리가 조회됩니다.
최상위 카테고리 ID를 기준으로 조회/응답 됩니다.

#### CategoryResponse Body
|필드명|타입|설명|
|---|---|-------|
|code|Long|결과 코드 값|
|message|String|응답 message|
|data|List|응답 data|

#### CategoryResponse - Data
|필드명| 타입            | 설명         |
|---|---------------|------------|
|id| Long          | 카테고리 ID    |
|name| String        | 카테고리 명     |
|parent| Long          | 상위 카테고리 ID |
|subCategories| List | 하위 카테고리 정보 |

#### 응답 바디 샘플
```
HTTP/1.1 200 OK
Content-Type: application/json

{
    "code": 0,
    "message": "success",
    "data": [
        {
            "id": 1,
            "name": "럭셔리",
            "parent": 0,
            "subCategories":null
        }
    ]
}
```

<br/>
<br/>

## 카테고리 등록

### 요청
```
POST http://127.0.0.1:8081/category?name={name}&parent={parent}
```

#### 요청 파라미터
| 필드명    | 타입     | 필수여부 | 설명                              |
|--------|--------|------|---------------------------------|
| name   | String | Y    | 새로운 카테고리 명을 입력해주세요.             |
| parent | Long   | Y    | 등록하려는 카테고리의 상위 카테고리 ID를 입력해주세요. |

### 응답
요청이 성공하면, 응답 바디에 처리 결과가 조회됩니다.

#### CategoryResponse Body
|필드명|타입|설명|
|---|---|-------|
|code|Long|결과 코드 값|
|message|String|응답 message|
|data|List|응답 data|


#### 응답 바디 샘플
```
HTTP/1.1 200 OK
Content-Type: application/json

{
    "code": 0,
    "message": "success",
    "data": ""
}
```

<br/>
<br/>

## 카테고리 삭제

### 요청
```
DELETE http://127.0.0.1:8081/category?id={id}
```

#### 요청 파라미터
| 필드명  | 타입     | 필수여부 | 설명                   |
|------|--------|------|----------------------|
| id   | Long   | Y    | 삭제할 카테고리 ID를 입력해주세요. |

### 응답
요청이 성공하면, 응답 바디에 처리 결과가 조회됩니다.

#### CategoryResponse Body
|필드명|타입|설명|
|---|---|-------|
|code|Long|결과 코드 값|
|message|String|응답 message|
|data|List|응답 data|


#### 응답 바디 샘플
```
HTTP/1.1 200 OK
Content-Type: application/json

{
    "code": 0,
    "message": "success",
    "data": ""
}
```

<br/>
<br/>

## 카테고리 수정

### 요청
```
PATCH http://127.0.0.1:8081/category/{category id}?name={name}&parent={parent}

요청 파라미터는 수정시 변경할 항목만 전달해주세요.
```

#### 요청 파라미터
| 필드명  | 타입     | 필수여부 | 설명                      |
|------|--------|------|-------------------------|
| name | String | N    | 변경될 카테고리 명을 입력해주세요.     |
| id   | Long   | N    | 변경될 상위 카테고리 ID를 입력해주세요. |

### 응답
요청이 성공하면, 응답 바디에 처리 결과가 조회됩니다.

#### CategoryResponse Body
|필드명|타입|설명|
|---|---|-------|
|code|Long|결과 코드 값|
|message|String|응답 message|
|data|List|응답 data|

#### CategoryResponse - Data
|필드명| 타입            | 설명             |
|---|---------------|----------------|
|id| Long          | 카테고리 ID        |
|name| String        | 변경된 카테고리 명     |
|parent| Long          | 변경된 상위 카테고리 ID |

#### 응답 바디 샘플
```
HTTP/1.1 200 OK
Content-Type: application/json

{
    "code": 0,
    "message": "success",
    "data": {
        "id": 1,
        "name": "수정",
        "parent": 10
    }
}
```
<br/><br/>
